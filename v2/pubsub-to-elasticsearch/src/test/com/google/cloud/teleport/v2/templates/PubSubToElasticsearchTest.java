/*
 * Copyright (C) 2019 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.google.cloud.teleport.v2.templates;

import com.google.common.collect.ImmutableList;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubMessage;
import org.apache.beam.sdk.options.ValueProvider;
import org.apache.beam.sdk.testing.NeedsRunner;
import org.apache.beam.sdk.testing.PAssert;
import org.apache.beam.sdk.testing.TestPipeline;
import org.apache.beam.sdk.transforms.Count;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Test class for {@link PubSubToElasticsearch}. */
@RunWith(JUnit4.class)
public class PubSubToElasticsearchTest {
  private static List<PubsubMessage> goodTestMessages;
  private static List<PubsubMessage> badTestMessages;
  private static List<PubsubMessage> nonFilterableTestMessages;
  private static List<PubsubMessage> allTestMessages;
  private static final String FILTER_KEY = "color";
  private static final String FILTER_VALUE = "red";

  @Before
  public void setUp() throws Exception {
    Map<String, String> testAttributeMap1 =
        new HashMap<String, String>() {
          {
            put("location", "GJ");
            put("name", "Shubh");
            put("age", "26");
            put("color", "white");
            put("coffee", "filter");
          }
        };
    Map<String, String> testAttributeMap2 =
        new HashMap<String, String>() {
          {
            put("location", "Durban");
            put("name", "Dan");
            put("age", "22");
            put("color", "red");
            put("coffee", "brown");
          }
        };

    goodTestMessages =
        ImmutableList.of(
            makePubsubMessage(
                "{\"location\":\"IN\", \"name\":\"John\", \"age\":\"28\", \"color\":\"red\", \"coffee\":\"cappuccino\"}",
                FILTER_KEY,
                FILTER_VALUE),
            makePubsubMessage(
                "{\"location\":\"US\", \"name\":\"Jane\", \"age\":\"29\", \"color\":\"red\", \"coffee\":\"black\"}",
                FILTER_KEY,
                FILTER_VALUE),
            makePubsubMessage(
                "{\"location\":\"UK\", \"name\":\"Smith\", \"age\":\"30\", \"color\":\"red\", \"coffee\":\"LATTE\"}",
                FILTER_KEY,
                FILTER_VALUE),
            new PubsubMessage(
                "{\"location\":\"IN\", \"name\":\"John\", \"age\":\"28\", \"color\":\"red\", \"coffee\":\"cappuccino\"}"
                    .getBytes(),
                testAttributeMap2));

    nonFilterableTestMessages =
        ImmutableList.of(
            makePubsubMessage(
                "{\"location\":\"MH\", \"name\":\"Lara\", \"age\":\"35\", \"color\":\"black\", \"coffee\":\"mochachino\"}",
                null,
                null),
            new PubsubMessage("".getBytes(), testAttributeMap1));

    badTestMessages =
        ImmutableList.of(
            makePubsubMessage("This is a bad record", null, null),
            makePubsubMessage("with unknown attribute", "dummy", "value"));

    allTestMessages =
        ImmutableList.<PubsubMessage>builder()
            .addAll(goodTestMessages)
            .addAll(badTestMessages)
            .addAll(nonFilterableTestMessages)
            .build();
  }

  @Rule public final transient TestPipeline pipeline = TestPipeline.create();

  /** Tests whether all messages flow through when no filter is provided. */
  @Test
  @Category(NeedsRunner.class)
  public void testNoInputFilterProvided() {
    PubSubToElasticsearch.Options options =
        TestPipeline.testingPipelineOptions().as(PubSubToElasticsearch.Options.class);
    PCollection<Long> pc =
        pipeline
            .apply(Create.of(allTestMessages))
            .apply(ParDo.of(new PubSubToElasticsearch.ExtractAndFilterEventsFn()))
            .apply(Count.globally());

    PAssert.thatSingleton(pc)
        .isEqualTo((long) goodTestMessages.size() + nonFilterableTestMessages.size());

    pipeline.run(options);
  }

  /** Tests whether only the valid messages flow through when a filter is provided. */
  @Test
  @Category(NeedsRunner.class)
  public void testInputFilterProvided() {
    PubSubToElasticsearch.Options options =
        TestPipeline.testingPipelineOptions().as(PubSubToElasticsearch.Options.class);
    options.setFilterKey(FILTER_KEY);
    options.setFilterValue(FILTER_VALUE);
    PCollection<Long> pc =
        pipeline
            .apply(Create.of(allTestMessages))
            .apply(ParDo.of(new PubSubToElasticsearch.ExtractAndFilterEventsFn()))
            .apply(Count.globally());

    PAssert.thatSingleton(pc).isEqualTo((long) goodTestMessages.size());

    pipeline.run(options);
  }

  private static PubsubMessage makePubsubMessage(
      String payloadString, String attributeKey, String attributeValue) {
    Map<String, String> attributeMap;
    if (attributeKey != null) {
      attributeMap = Collections.singletonMap(attributeKey, attributeValue);
    } else {
      attributeMap = Collections.EMPTY_MAP;
    }
    return new PubsubMessage(payloadString.getBytes(), attributeMap);
  }
}
