/*
 * Copyright (C) 2019 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.google.cloud.teleport.v2.templates;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.PipelineResult;
import org.apache.beam.sdk.io.elasticsearch.ElasticsearchIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubMessage;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.options.StreamingOptions;
import org.apache.beam.sdk.options.Validation;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link PubSubToElasticsearch} pipeline is a streaming pipeline which ingests data in JSON
 * format from PubSub, filters the data based on an incoming key and value pair, and inserts as an
 * index, the resulting records to Elasticsearch. If the record is not a valid json or it does not
 * pass the filter, it is plainly ignored and the next record is taken up for processing.
 *
 * <p><b>Pipeline Requirements</b>
 *
 * <ul>
 *   <li>The PubSub topic and subscriptions exist
 *   <li>The Elasticsearch is up and running
 * </ul>
 *
 * <p><b>Example Usage</b>
 *
 * <pre>
 * # Set the pipeline vars
 * PROJECT_NAME=my-project
 * BUCKET_NAME=my-bucket
 * INPUT_SUBSCRIPTION=my-subscription
 * TEMPLATES_LAUNCH_API="${API_ROOT_URL}/v1b3/projects/${PROJECT_NAME}/templates:launch"
 * ELASTICSEARCH_INDEX_NAME=my-index
 * ELASTICSEARCH_HOSTNAME=my-host:port
 * DOCUMENT_TYPE=my-doc
 *
 * # Set containerization vars
 * IMAGE_NAME=my-image-name
 * TARGET_GCR_IMAGE=gcr.io/${PROJECT}/${IMAGE_NAME}
 * BASE_CONTAINER_IMAGE=my-base-container-image
 * BASE_CONTAINER_IMAGE_VERSION=my-base-container-image-version
 * APP_ROOT=/path/to/app-root
 * COMMAND_SPEC=/path/to/command-spec
 *
 * # Build and upload image
 * mvn clean package \
 * -Dimage=${TARGET_GCR_IMAGE} \
 * -Dbase-container-image=${BASE_CONTAINER_IMAGE} \
 * -Dbase-container-image.version=${BASE_CONTAINER_IMAGE_VERSION} \
 * -Dapp-root=${APP_ROOT} \
 * -Dcommand-spec=${COMMAND_SPEC}
 *
 * # Create an image spec in GCS that contains the path to the image
 * {
 *   "docker_template_spec": {
 *      "docker_image": $TARGET_GCR_IMAGE
 *    }
 * }
 *
 * # Execute template:
 * API_ROOT_URL="https://dataflow.googleapis.com"
 * TEMPLATES_LAUNCH_API="${API_ROOT_URL}/v1b3/projects/${PROJECT_NAME}/templates:launch"
 * JOB_NAME="pubsub-to-elasticsearch-`date +%Y%m%d-%H%M%S-%N`"
 *
 * time curl -X POST \
 *     -H "Content-Type: application/json" \
 *     -H "Authorization: Bearer $(gcloud auth print-access-token)" \
 *     "${TEMPLATES_LAUNCH_API}"`\
 *     `"?validateOnly=false"` \
 *     `"&dynamicTemplate.gcsPath=gs://${BUCKET_NAME}/pubsub-to-elasticsearch-image-spec.json"` \
 *     `"&dynamicTemplate.stagingLocation=gs://${BUCKET_NAME}/staging" \
 *     -d
 *     '{
 *         "jobName":"$JOB_NAME",
 *         "parameters": {
 *             "inputSubscription":"$INPUT_SUBSCRIPTION",
 *             "outputIndex":"$ELASTICSEARCH_INDEX_NAME",
 *             "elasticsearchAddresses":"$ELASTICSEARCH_HOSTNAME",
 *             "documentType":"$DOCUMENT_TYPE"
 *         }
 *     }'
 * </pre>
 */
public class PubSubToElasticsearch {

  /**
   * The {@link PubSubToElasticsearch}
   *
   * <p>Inherits standard configuration options.
   */
  public interface Options extends PipelineOptions, StreamingOptions {
    @Description(
        "The Cloud Pub/Sub subscription to consume from."
            + "The name should be in the format of "
            + "projects/<project-id>/subscriptions/<subscriptions-name>")
    @Validation.Required
    String getInputSubscription();

    void setInputSubscription(String inputSubscription);

    @Description("The Elastic index to index the Documents to")
    @Validation.Required
    String getOutputIndex();

    void setOutputIndex(String outputIndex);

    @Description(
        "The host addresses of the ElasticSearch"
            + "Multiple addresses to be specified with a comma separated value e.g."
            + "host1:port,host2:port,host3:port")
    @Validation.Required
    String getElasticsearchAddresses();

    void setElasticsearchAddresses(String elasticsearchAddresses);

    @Description(
        "Filter events based on an optional attribute key. "
            + "No filters are applied if a filterKey is not specified.")
    @Validation.Required
    String getFilterKey();

    void setFilterKey(String filterKey);

    @Description(
        "Filter attribute value to use in case a filterKey is provided. "
            + "A null filterValue is used by default.")
    @Validation.Required
    String getFilterValue();

    void setFilterValue(String filterValue);

    @Description("The document type of the document to index")
    @Validation.Required
    String getDocumentType();

    void setDocumentType(String documentType);
  }

  /**
   * DoFn that will determine if events are to be filtered. If filtering is enabled, it will only
   * index events that pass the filter else, it will index all input events.
   */
  public static class ExtractAndFilterEventsFn extends DoFn<PubsubMessage, String> {

    private Boolean doFilter;
    private String inputFilterKey;
    private String inputFilterValue;
    private static final Logger LOG = LoggerFactory.getLogger(ExtractAndFilterEventsFn.class);

    @StartBundle
    public void startBundle(StartBundleContext context) {
      if (this.doFilter != null) {
        return; // Filter has been evaluated already
      }

      Options options = context.getPipelineOptions().as(Options.class);

      inputFilterKey = (options.getFilterKey() == null ? null : options.getFilterKey());
      inputFilterValue = (options.getFilterValue() == null ? null : options.getFilterValue());

      if (inputFilterKey == null) {
        this.doFilter = false;
      } else {
        this.doFilter = true;
        LOG.info(
            "Enabling event filter [key: " + inputFilterKey + "][value: " + inputFilterValue + "]");
      }
    }

    @ProcessElement
    public void processElement(ProcessContext context) {
      String message =
          context.element().getPayload().length > 0
              ? new String(context.element().getPayload())
              : context.element().getAttributeMap().toString();
      JsonObject messageObject = null;
      try {
        messageObject = new Gson().fromJson(message, JsonObject.class);
      } catch (JsonSyntaxException e) {
        LOG.warn("Input message is not a valid Json record, ignoring");
      }

      if (messageObject == null) {
      } else if (!this.doFilter) {
        // Filter is not enabled
        context.output(messageObject.toString());
      } else {
        if (messageObject.has(this.inputFilterKey)
            && messageObject.get(this.inputFilterKey).getAsString().equals(this.inputFilterValue)) {
          context.output(messageObject.toString());
        }
      }
    }
  }

  /**
   * Main entry point for executing the pipeline.
   *
   * @param args The command-line arguments to the pipeline.
   */
  public static void main(String[] args) {

    // Parse the user options passed from the command-line.
    Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);

    run(options);
  }

  /**
   * Runs the pipeline with the supplied options.
   *
   * @param options The execution parameters to the pipeline.
   * @return The result of the pipeline execution.
   */
  public static PipelineResult run(Options options) {

    // Create the pipeline
    Pipeline pipeline = Pipeline.create(options);

    /**
     * Steps: 1) Read PubSubMessage with attributes from input PubSub subscription.
     *        2) Apply any filters if an attribute=value pair is provided.
     *        3) Index PubSubMessage to output ES index.
     */
    pipeline
        /**
         * Step 1) Read PubSubMessage with attributes from input PubSub subscription.
         */
        .apply(
            "Read PubSub Events",
            PubsubIO.readMessagesWithAttributes().fromSubscription(options.getInputSubscription()))

        /**
         * Step 2) Apply any filters if an attribute=value pair is provided.
         */
        .apply("Filter Events If Enabled", ParDo.of(new ExtractAndFilterEventsFn()))
        /**
         * Step 3) Index PubSubMessage to output ES index.
         */
        .apply(
            "Index PubSub Events",
            ElasticsearchIO.write()
                .withConnectionConfiguration(
                    ElasticsearchIO.ConnectionConfiguration.create(
                        options.getElasticsearchAddresses().split(","),
                        options.getOutputIndex(),
                        options.getDocumentType())));

    // Execute the pipeline and return the result.
    return pipeline.run();
  }
}
